package dreamteam.iter;

import android.graphics.Bitmap;

/**
 * Created by Arkkienkeli on 24.01.2016.
 */
public class Place {

        private Bitmap image;
        private String title;

        public Place(Bitmap image, String title) {
            super();
            this.image = image;
            this.title = title;
        }

        public Bitmap getImage() {
            return image;
        }

        public void setImage(Bitmap image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
}
