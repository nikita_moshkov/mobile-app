package dreamteam.iter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import dreamteam.iter.Set;
import java.util.List;

/**
 * Created by Arkkienkeli on 13.12.2015.
 */
public class SetAdapter extends ArrayAdapter<Set> {

    private List<Set> itemList;
    private Context context;

    public SetAdapter(List<Set> itemList, Context ctx) {
        super(ctx, android.R.layout.simple_list_item_2, itemList);
        this.itemList = itemList;
        this.context = ctx;
    }

    public int getCount() {
        if (itemList != null)
            return itemList.size();
        return 0;
    }

    public Set getItem(int position) {
        if (itemList != null)
            return itemList.get(position);
        return null;
    }

    public long getItemId(int position) {
        if (itemList != null)
            return itemList.get(position).hashCode();
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item,  parent, false);
        }

        Set s = itemList.get(position);
        TextView text = (TextView) v.findViewById(R.id.name);
        text.setText(s.getName());

        TextView text1 = (TextView) v.findViewById(R.id.description);
        text1.setText("\n" + s.getDescription());


        return v;

    }

    public List<Set> getItemList() {
        return itemList;
    }

    public void setItemList(List<Set> itemList) {
        this.itemList = itemList;
    }

}