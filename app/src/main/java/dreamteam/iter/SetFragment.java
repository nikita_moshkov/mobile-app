package dreamteam.iter;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SetFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    protected SetAdapter adpt;
    protected View myFragmentView;

    public SetFragment() {
        // Required empty public constructor
    }

    public static SetFragment newInstance(String param1, String param2) {
        SetFragment fragment = new SetFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("SET FRAG");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.fragment_set, container, false);
        SetAdapter adpt = new SetAdapter(new ArrayList<Set>(), getActivity());
        Button searchButton = (Button) myFragmentView.findViewById(R.id.search_button);
        ListView lView = (ListView) myFragmentView.findViewById(R.id.listView);
        lView.setAdapter(adpt);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
        return myFragmentView;
    }


        // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void search() {
        System.out.println("search");
        Button searchButton = (Button) getView().findViewById(R.id.search_button);
        EditText searchText = (EditText) getView().findViewById(R.id.searchField);
        searchButton.setEnabled(true);
        String city = searchText.getText().toString();
        (new RestAcesss()).execute(city);
    }


    private class RestAcesss extends AsyncTask<String, Void, List<Set>> {

        @Override
        protected void onPostExecute(List<Set> result) {
            super.onPostExecute(result);
            SetAdapter adpt  = new SetAdapter(new ArrayList<Set>(),getActivity().getApplicationContext());
            ListView lView = (ListView) getView().findViewById(R.id.listView);
            lView.setAdapter(adpt);
            System.out.println(adpt);
            adpt.setItemList(result);
            adpt.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Set> doInBackground(String... params) {
            List<Set> result = new ArrayList<Set>();
            try {
                String url = "http://192.168.56.102:5000/set/list";
                URL object = new URL(url);

                HttpURLConnection con = (HttpURLConnection) object.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");

                JSONObject ci = new JSONObject();
                ci.put("city", params[0]);

                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                wr.write(ci.toString());
                wr.flush();

                System.out.println(ci.toString());

                StringBuilder sb = new StringBuilder();
                int HttpResult = con.getResponseCode();
                if(HttpResult == HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }

                    br.close();

                    System.out.println(sb.toString());
                    JSONObject jsonObject = new JSONObject(sb.toString());
                    //JSONArray arr = new JSONArray(jsonObject);

                    List<String> l = new ArrayList<String>();
                    JSONArray arr = jsonObject.getJSONArray("routes");
                    System.out.println("Arr" + arr);
                    for (int i=0; i < arr.length(); i++) {
                        l.add(arr.getString(i));
                        //l.add(arr.getJSONObject(i).toString());
                    }
                    for (int i=0; i < l.size(); i++) {
                        result.add(convertSet(new JSONObject(l.get(i))));
                    }
                    System.out.println(result.get(0).getName());
                    return result;

                }else{
                    System.out.println(con.getResponseMessage());
                    return null;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        private Set convertSet(JSONObject obj) throws JSONException {
            String name = obj.getString("name");
            String description = obj.getString("description");
            return new Set(name, description);
        }
    }
}
