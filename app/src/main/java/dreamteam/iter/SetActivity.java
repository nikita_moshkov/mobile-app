package dreamteam.iter;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringEscapeUtils;

public class SetActivity extends AppCompatActivity {
    protected SetAdapter adpt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set);
        System.out.println("Adapterb4 " + adpt);
        Button searchButton = (Button) findViewById(R.id.search_button);
        SetAdapter adpt  = new SetAdapter(new ArrayList<Set>(), this);
        System.out.println("Adapter" + adpt);
        ListView lView = (ListView) findViewById(R.id.listView);
        lView.setAdapter(adpt);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });


    }


    public void search() {

        Button searchButton = (Button) findViewById(R.id.search_button);
        EditText searchText = (EditText) findViewById(R.id.searchField);
        searchButton.setEnabled(false);
        String city = searchText.getText().toString();
        (new RestAcesss()).execute(city);
    }


    private class RestAcesss extends AsyncTask<String, Void, List<Set>> {

        @Override
        protected void onPostExecute(List<Set> result) {
            super.onPostExecute(result);
            SetAdapter adpt  = new SetAdapter(new ArrayList<Set>(), getApplicationContext());
            ListView lView = (ListView) findViewById(R.id.listView);
            lView.setAdapter(adpt);
            System.out.println(adpt);
            adpt.setItemList(result);
            adpt.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Set> doInBackground(String... params) {
            List<Set> result = new ArrayList<Set>();
            try {
                String url = "http://192.168.56.102:5000/set/list";
                URL object = new URL(url);

                HttpURLConnection con = (HttpURLConnection) object.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");

                JSONObject ci = new JSONObject();
                ci.put("city", params[0]);

                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                wr.write(ci.toString());
                wr.flush();

                System.out.println(ci.toString());

                StringBuilder sb = new StringBuilder();
                int HttpResult = con.getResponseCode();
                if(HttpResult == HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }

                    br.close();

                    System.out.println(sb.toString());
                    JSONObject jsonObject = new JSONObject(sb.toString());
                    //JSONArray arr = new JSONArray(jsonObject);

                    List<String> l = new ArrayList<String>();
                    JSONArray arr = jsonObject.getJSONArray("routes");
                    System.out.println("Arr" + arr);
                    for (int i=0; i < arr.length(); i++) {
                        l.add(arr.getString(i));
                        //l.add(arr.getJSONObject(i).toString());
                    }
                    for (int i=0; i < l.size(); i++) {
                        result.add(convertSet(new JSONObject(l.get(i))));
                    }
                    System.out.println(result.get(0).getName());
                    return result;

                }else{
                    System.out.println(con.getResponseMessage());
                    return null;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        private Set convertSet(JSONObject obj) throws JSONException {
            String name = obj.getString("name");
            String description = obj.getString("description");
            return new Set(name, description);
        }
    }

}
