package dreamteam.iter;

/**
 * Created by Arkkienkeli on 13.12.2015.
 */
public class Set {
    public String name;
    public String description;

    public Set(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}